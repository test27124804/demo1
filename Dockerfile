FROM node

WORKDIR /src


COPY . .

EXPOSE 8006:8006

CMD node server.js